(ns sicp-clj.study-1
  (:import java.lang.Math))


;; ======================================== 1.3
(defn BIG-NUM-LIST [& args]
  (let [ A (apply list args)
        T (first A)
        B (reverse (sort (rest A))) ]
    ;; racket 와 다르게, take함수의 리스트 갯수 취하는 (인자-숫자) 가 first인자(T) 네요
    (take T B) ))

;; ELISP처럼 sort는 구현하지 않고 (함수가 내장되어 있다.)
;; (filter #(< % 8) '(3 10)) 

;; 함수 TEST
(BIG-NUM-LIST 2 3 5 10 8) 


;; 수학 연산 함수
;; 클로저는 Math는 JVM?에 의존하나 봄.
(defn infinite-PARABOLOID [x y]
  (+ (Math/pow x 2) (Math/pow y 2)) )



(let [LIST (BIG-NUM-LIST 2 3 5 10 8) ;; 함수(BIG-NUM-LIST) 필요
      x (first LIST)
      y (second LIST)]
  (infinite-PARABOLOID x y)
  )

;;------ 조금 다르게 풀기 ------

;; (BIG-NUM-LIST) 함수 없이~
(let [SQUARE (map #(Math/pow % 2) '(3 5 10 8)) ;; 힌트 주신 전부 제곱 시킴
      LIST (reverse (sort SQUARE))  ;; 제곱된 리스트 큰것대로 정렬
      x (first LIST)  
      y (second LIST)]
  (+ x y) ) 

;;------- https://github.com/gregsexton/SICP-Clojure/blob/master/src/sicp/ch1.clj
;; 위 아저씨 문제 풀이 (letfn 을 사용) 
(defn sum-square-larger [a b c] 
  (letfn [(square [n] (* n n))] 
    (- (apply + (map square (vector a b c))) 
       (square (min a b c)))))

(sum-square-larger 10 3 8)


;; 자바함수 활용하면 이렇겠지요
(defn sum-square-lg [a b c]
  (- (reduce (fn [result current]
               (+ (Math/pow current 2) result))
             [a b c])
     (Math/pow (min a b c) 2)))


